require 'roar/json'

module HatRequestRepresenter
  include Roar::JSON
  property :subject
  property :object
  collection :evidence
end
