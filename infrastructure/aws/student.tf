# These are variables that you must fill in before you can deploy infrastructure.

variable "student_id" {
  description = "A unique ID that separates your resources from everyone else's"
  default     = "12345"
}

variable "aws_region" {
  description = "The AWS Region to use"
  default     = "eu-central-1"
}

variable "database_username" {
  description = "Your chosen master user name for the database. Must be less than 16 characters and may only use a-z, A-Z, 0-9, '$' and '_'. The first character cannot be a digit."
  default     = "masterofsecrets"
}

variable "database_password" {
  description = "Your chosen master user password for the database. Must be at least 8 characters, but not contain '/', '\\', \", or '@'."
  default     = "A1secret"
}

variable "public_key" {
  description = "The public half of your SSH key. Make sure it is in OpenSSH format (it should start with 'ssh', not 'BEGIN PUBLIC KEY')"
  default     = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDjqOoiAG1tmVPQKKVM9dekKkU2lUHs7Hikhcp0T4f12YeKo6Un0a/f8chyn+KxV9h3+URPGwQIe3LMtvUpJH55lnvGmWBOperXvbikC6BtuyPWR0UsfuBtsw1wxXWUr6gHOE/eAjuOJUXYExEL/1EdAeLwq1f5hSky7+bqdEt896dwZMhh7j5hAdEZ6tHtJ8Ov39adGkUM/0OU/I1i4/BywKKyne7uyk0ZhjbGMDBLqFTGtcr1dRBA8vCDV3Z5CZSFJqs4ed34+ywIgVWJ1EagUKUsSdDDE1sL/ixLW/hpUIShJHNi08XFR+5Yo2M1M/VaFP1+v0Z+AY9uU0nV/Sgb mtnyg@Dipole"
}
