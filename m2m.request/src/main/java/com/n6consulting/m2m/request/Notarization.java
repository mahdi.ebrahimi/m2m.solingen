package com.n6consulting.m2m.request;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class Notarization {
  @Id
  @GeneratedValue
  private Long id;

  private String principal;
  private String reason;

  public Long getId() {
    return id;
  }

  public String getPrincipal() {
    return principal;
  }

  public String getReason() {
    return reason;
  }
}
